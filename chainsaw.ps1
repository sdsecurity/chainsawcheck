# This script will download chainsaw and unzip it if it's not already done so and then execute a full hunt against windows event logs

$chainsawfilepath = 'C:\Windows\Temp\chainsaw\chainsaw.exe'
$workingdir = 'C:\Windows\Temp'
$PSDefaultParameterValues['Out-File:Encoding'] = 'utf8'

if (-not(Test-Path -Path $chainsawfilepath)){
    try {
        Write-Host "Downloading chainsaw" -ForegroundColor Yellow
        Invoke-WebRequest -Uri "https://github.com/countercept/chainsaw/releases/download/v1.0.2/chainsaw_x86_64-pc-windows-msvc.zip" -OutFile "$workingdir\chainsaw.zip"
        Write-Host "Done" -ForegroundColor Green
        Write-Host "Extracting chainsaw from zip file" -ForegroundColor Yellow
        Expand-Archive -LiteralPath $workingdir\chainsaw.zip -DestinationPath $workingdir
        Write-Host "Done" -ForegroundColor Green
        Remove-Item -Path $workingdir\chainsaw.zip
        cd $workingdir\chainsaw
        .\chainsaw.exe hunt C:\Windows\System32\winevt\Logs\ --rules .\sigma_rules --mapping .\mapping_files\sigma-mapping.yml
    }
    catch {
        throw $_.Exception.Message
        Write-Host "Couldn't download the file"
    }
}


else {
    Write-Host "Chainsaw is already downloaded. Starting execution" -ForegroundColor Green
    cd $workingdir\chainsaw
    .\chainsaw.exe hunt C:\Windows\System32\winevt\Logs\ --rules .\sigma_rules --mapping .\mapping_files\sigma-mapping.yml
}